//
//  Category.h
//  JamTreeTabBasedApp
//
//  Created by mobytz004 on 22/07/13.
//  Copyright (c) 2013 Mobytz. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Category : NSManagedObject

@property (nonatomic, retain) NSNumber * catItem;
@property (nonatomic, retain) NSNumber * deleted;
@property (nonatomic, retain) NSString * desc;
@property (nonatomic, retain) NSString * image;
@property (nonatomic, retain) NSString * name;

@end
