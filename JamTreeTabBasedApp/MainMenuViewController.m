//
//  MainMenuViewController.m
//  JamTreeMockApp
//
//  Created by Mobytz on 06/06/13.
//  Copyright (c) 2013 Mobytz. All rights reserved.
//

#import "MainMenuViewController.h"
#import "menuView.h"
#import "MainMenuSub1ViewController.h"
#import "TabViewController.h"
#import "Category.h"
#import "MenuItems.h"
#import "WebService.h"
#import "sqliteManager.h"
#import "UIImageView+WebCache.h"


@interface MainMenuViewController ()
{
    int                         i,j;
    int                         count;
    NSTimer                    *timer1;
    NSTimer                    *timer2;
    NSTimer                    *timer3;
    MainMenuSub1ViewController *objectSub1;

    TabViewController          *tabObject;
    WebService *webService;
    //ObjectClass *obj;
}
@end

@implementation MainMenuViewController
@synthesize splashView;
@synthesize animationView;
@synthesize objectMenu,arr,obj,HUD;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    __unused int k;
    [super viewDidLoad];
    deleg = (AppDelegate*)[[UIApplication sharedApplication] delegate];
       
    // Do any additional setup after loading the view from its nib.
    i =0;
    j=0;
    page1Object                    = [[menuView alloc] init];
    objectMenu                               = page1Object;
    objectMenu.objectMainMenuController      = self;
    
    objectSub1 = [[MainMenuSub1ViewController alloc] initWithNibName:@"MainMenuSub1ViewController" bundle:nil];
       count           =  arr.count;
    [self.mainScrollView setContentSize:CGSizeMake(320, 1000)];
    
    webService = [[WebService alloc]init];
    [self getCategoryData];
    [self getMenuItemsData];
    if (([deleg.categoryDictnry count]==0)&&([deleg.menuItemsDictnry count]==0) ) {
        [webService fetchMenu];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(doParsing:) name:NOTIF_DATAFETCH object:nil];
    }
    
    
    float delay=.5f;
    for (int c=1; c<=[deleg.categoryDictnry count]/2; c++) {
        [self performSelector:@selector(menuAppear) withObject:nil afterDelay:delay];
        delay=delay+ .999f;
    }
    [self.mainScrollView setContentSize:CGSizeMake(320, [deleg.categoryDictnry count]*55)];
   
   
        HUD = [[MBProgressHUD alloc] initWithView:self.view];
        [self.view addSubview:HUD];
        [HUD show:YES];
        
    
 
    
}
-(void)doParsing:(NSNotification *)dict
{
    [HUD hide:YES];
    NSLog(@"payload dictttt%@",dict);
    NSArray *a=[dict valueForKey:@"userInfo"];
     NSLog(@"%@",a);
   deleg.tempDict=[a valueForKey:@"menucategory"];
   
     
    deleg.count  =  arr.count;
    [self saveCategoryData];
    [self saveMenuItemsData];
    [self getCategoryData];
    [self getMenuItemsData];
    float delay=.25;
    for (int c=1; c<=[deleg.categoryDictnry count]/2; c++) {
        [self performSelector:@selector(menuAppear) withObject:nil afterDelay:delay];
        delay=delay+1;
    }
    [self.mainScrollView setContentSize:CGSizeMake(320, [deleg.categoryDictnry count]*75)];
    
    
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:HUD];
    [HUD show:YES];
    

    
}
    -(void)saveCategoryData{
        for (int i=0; i<[deleg.tempDict count]; i++) {
    Category *category = [NSEntityDescription insertNewObjectForEntityForName:@"Category" inManagedObjectContext:deleg.managedObjectContext];

    NSMutableArray *nameArr=[deleg.tempDict valueForKey:@"name"];
    category.name= nameArr[i];
    NSMutableArray *imgArr=[deleg.tempDict valueForKey:@"image"];
    category.image= imgArr[i];
               NSMutableArray *idArr=[deleg.tempDict valueForKey:@"id"];
    category.catItem= idArr[i];
        NSMutableArray *descArr=[deleg.tempDict valueForKey:@"desc"];
        category.desc= descArr[i];
        NSMutableArray *deltArr=[deleg.tempDict valueForKey:@"deleted"] ;
        category.deleted= [NSNumber numberWithInt:deltArr[i]];
        
        [deleg saveContext];
        NSError *error = nil;
        
       
    }
 
    }

-(void)saveMenuItemsData{
    
    
    NSMutableDictionary *dict=[deleg.tempDict valueForKey:@"menuItems"];
    for (int k=0; k<[dict count]; k++) {
       
         NSMutableArray *nameArr=[dict valueForKey:@"name"];
        NSMutableArray *imgArr=[dict valueForKey:@"image"];
        NSMutableArray *idArr=[dict valueForKey:@"catId"];
        NSMutableArray *descArr=[dict valueForKey:@"desc"];
        NSMutableArray *deltArr=[dict valueForKey:@"deleted"] ;
        NSMutableArray *priceArr=[dict valueForKey:@"price"] ;
        NSMutableArray *itemIDArr=[dict valueForKey:@"id"] ;
        
        
        for (i=0; i<[nameArr count]; i++) {
           NSMutableArray *nameArr1=nameArr[i];
            NSMutableArray *imgArr1=imgArr[i];
            NSMutableArray *idArr1=idArr[i];

            NSMutableArray *descArr1=descArr[i];
            NSMutableArray *deltArr1=deltArr[i];

            NSMutableArray *priceArr1=priceArr[i];

            NSMutableArray *itemIDArr1=itemIDArr[i];


            
            for (int j=0; j< [nameArr1 count]; j++)
            {
                MenuItems *menuItem = [NSEntityDescription insertNewObjectForEntityForName:@"MenuItems" inManagedObjectContext:deleg.managedObjectContext];
                menuItem.name=nameArr1[j];
                menuItem.image=imgArr1[j];
                menuItem.catId= idArr1[j];
                menuItem.desc=descArr1[j];
                menuItem.deleted=[NSNumber numberWithInt:deltArr1[j] ];
                menuItem.price = priceArr1[j];
                menuItem.itemId=itemIDArr1[j];
            
            }
        }
        
         
        [deleg saveContext];
        }
        }




-(void)getCategoryData{
   [HUD hide:YES];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Category" inManagedObjectContext:deleg.managedObjectContext];
    [fetchRequest setEntity:entity];
    fetchRequest.returnsObjectsAsFaults=false;
    
    NSError *error = nil;
    NSArray *fetchedObjects = [[NSArray alloc] init];
    fetchedObjects = [deleg.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    if (fetchedObjects == nil) {
        NSLog(@"ghdfhh");
    }
    
   deleg.categoryDictnry= fetchedObjects;
    //[HUD hide:YES];

     }


-(void)getMenuItemsData{
    [HUD hide:YES];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"MenuItems" inManagedObjectContext:deleg.managedObjectContext];
    [fetchRequest setEntity:entity];
    fetchRequest.returnsObjectsAsFaults=false;

    NSError *error = nil;
    NSArray *fetchedObjects = [[NSArray alloc] init];
    fetchedObjects = [deleg.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    if (fetchedObjects == nil) {
        NSLog(@"ghdfhh");
    }
    
   deleg.menuItemsDictnry = fetchedObjects;
    
    //[HUD hide:YES];
}

-(void) menuAppear
{
    [objectMenu mainViewAnimating:self];
    
    [self.mainScrollView addSubview:objectMenu.mView];
}
-(void) menuButtonSelected:(int) value
{
    [self.navigationController pushViewController:objectSub1 animated:YES];
    objectSub1.value1=value;
    }

- (void)viewWillAppear:(BOOL)animated
{
    
    [super viewWillAppear:YES];
    [self.mainScrollView setContentOffset:CGPointMake(0, 0)];

}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
