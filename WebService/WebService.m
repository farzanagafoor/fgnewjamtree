//
//  WebService.m
//  bionicPhotoManager
//
//  Created by Zachariah on 11/03/2013.
//  Copyright (c) 2013 Kiran Babu. All rights reserved.
//

#import "WebService.h"
#import "AppDelegate.h"
#import "Global.h"
#import "MainMenuViewController.h"
@implementation WebService
@synthesize apiCallType,finished,arr;

-(void) fetchMenu
{
    
    NSString* service = @"fetchMenu";
    self.apiCallType = REQAPPDETAILS;
    NSURL *cerurl ;
    cerurl = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@?id=%@",webServiceLink,service,RESTAURANTID]];
    NSLog(@"URL->%@",cerurl);
    request = [ASIHTTPRequest requestWithURL:cerurl];
    [request setRequestMethod:@"GET"];
    [request setUseSessionPersistence:NO];
    [request setDelegate:self];
	[request startAsynchronous];
    
   }

- (void)requestFinished:(ASIHTTPRequest *)_request
{
    str=[[NSString alloc]init];
 
    str=[_request responseString];
    NSLog(@"fetMenuData   %@",str);
    
    
        
    
    switch (self.apiCallType)
    {
        case REQAPPDETAILS:
        {
           NSDictionary *payloadDict = [[CJSONDeserializer deserializer] deserialize:[_request responseData] error:&theError];
        [[NSNotificationCenter defaultCenter] postNotificationName:NOTIF_DATAFETCH object:nil userInfo:payloadDict];
           NSLog(@"%@",payloadDict);
            
                break;
        }
        
        default:
            break;
    }
    finished=YES;
    
}


- (void)requestFailed:(ASIHTTPRequest *)request
{
    NSLog(@"asdad");
    NSError *error = [request error];
    
   // [[NSNotificationCenter defaultCenter] postNotificationName:NOTIF_CONNECTION_ERROR object:nil userInfo:nil];
    finished=YES;
}
@end
