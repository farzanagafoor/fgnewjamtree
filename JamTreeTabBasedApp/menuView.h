//
//  menuView.h
//  JamTreeMockApp
//
//  Created by Mobytz on 06/06/13.
//  Copyright (c) 2013 Mobytz. All rights reserved.
//

#import <UIKit/UIKit.h>



@class MainMenuViewController, MainMenuSub1ViewController;

@interface menuView : UIView
{
    MainMenuViewController      *objectMainMenuController;
    MainMenuSub1ViewController  *objectSub1;
   
    
    UIView *mView ;
    UIButton *mButton;
    UILabel *mLabel;
    UIView *sView;
    UIButton *sButton;
    UILabel *sLabel;
    UILabel *ssLabel;
    UIImageView *sImgView;
      int k;
    NSString *imgpath;
    NSURL *url;
    NSData *imgData;
    NSString *imageName;
    
}   

@property (strong, nonatomic) IBOutlet menuView             *menuPage;
@property (nonatomic, retain) MainMenuViewController        *objectMainMenuController;
@property (nonatomic, retain) MainMenuSub1ViewController *objectSub1;
@property (nonatomic, retain) UIView *mView ;
@property (nonatomic, retain) UIButton *mButton;
@property (nonatomic, retain) IBOutlet UIView *sView ;
@property (nonatomic, retain) IBOutlet UIButton *sButton;
@property (nonatomic, retain) IBOutlet UILabel *sLabel;
@property (nonatomic, retain) IBOutlet UILabel *ssLabel;
@property (nonatomic, retain) IBOutlet UIImageView *sImgView;
@property (nonatomic) int k;
- (IBAction) menuButtonPressed:(id)sender;
- (IBAction)mainViewAnimating:(id) sender;
-(void)subMenuAnimating:(int)value;
-(void)sub4MenuAnimating:(int) count;
-(void) sub6MenuAnimating:(int) count;


@end
