//
//  FacebookViewController.m
//  JamTreeTabBasedApp
//
//  Created by Mobytz on 01/07/13.
//  Copyright (c) 2013 Mobytz. All rights reserved.
//
#import "FeedCell.h"
#import "FacebookViewController.h"
#import "AppDelegate.h"
#import "SBJSON.h"
#import "FbGraphFile.h"
#import "UIImageView+WebCache.h"
@class HJManagedImageV;
@interface FacebookViewController ()

@end

@implementation FacebookViewController
@synthesize fbGraph,feedPostId,table;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];

//    [[NSNotificationCenter defaultCenter]
//     addObserver:self
//     selector:@selector(sessionStateChanged:)
//     name:FBSessionStateChangedNotification
//     object:nil];

    // Do any additional setup after loading the view from its nib.
}
- (void)viewDidAppear:(BOOL)animated {
	
	/*Facebook Application ID*/
	NSString *client_id = @"137648846423858";
	
	//alloc and initalize our FbGraph instance
	self.fbGraph = [[FbGraph alloc] initWithFbClientID:client_id];
	
	//begin the authentication process.....
	[fbGraph authenticateUserWithCallbackObject:self andSelector:@selector(fbGraphCallback:)
						 andExtendedPermissions:@"user_photos,user_videos,publish_stream,offline_access,user_checkins,friends_checkins,read_stream"];
    
    
    
    
    }
- (void)fbGraphCallback:(id)sender {
	
	if ( (fbGraph.accessToken == nil) || ([fbGraph.accessToken length] == 0) ) {
		
		NSLog(@"You pressed the 'cancel' or 'Dont Allow' button, you are NOT logged into Facebook...I require you to be logged in & approve access before you can do anything useful....");
		
		//restart the authentication process.....
		[fbGraph authenticateUserWithCallbackObject:self andSelector:@selector(fbGraphCallback:)
							 andExtendedPermissions:@"user_photos,user_videos,publish_stream,offline_access,user_checkins,friends_checkins"];
		
	} else {
		//pop a message letting them know most of the info will be dumped in the log
//		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Note" message:@"For the simplest code, I've written all output to the 'Debugger Console'." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
//		[alert show];
//		
		
		NSLog(@"------------>CONGRATULATIONS<------------, You're logged into Facebook...  Your oAuth token is:  %@", fbGraph.accessToken);
		
	}
	FbGraphResponse *fb_graph_response = [fbGraph doGraphGet:@"me/home" withGetVars:nil];
    //[facebook requestWithGraphPath:@"me/home" andDelegate:self];
	//NSLog(@"getMeFeedButtonPressed:  %@", fb_graph_response.htmlResponse);
    SBJsonParser *parser = [[SBJsonParser alloc] init];
    
    NSDictionary *dict= [parser objectWithString:fb_graph_response.htmlResponse];
    // NSLog(@"ddddddddddddddicccccc %@",dict);
    arr=[dict objectForKey:@"data"];
    // NSLog(@"\n\n\n\n\n\n\nn\n\nn\n\nn\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\narrrrrrrrrraaayyyyyyyyyyyy %@",arr);
    counnt=[arr count];
    NSLog(@"%i count",counnt);
	[table reloadData];

}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath

{
    static NSString *CellIdentifier = @"FeedCell";
    
	FeedCell *feedCell = (FeedCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
	if (feedCell == nil)
	{
		NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:CellIdentifier owner:self options:nil];
        
		for (id currentObject in topLevelObjects)
		{
			if ([currentObject isKindOfClass:[UITableViewCell class]])
			{
				feedCell =  (FeedCell *) currentObject;
				break;
			}
		}
    }
    
    dic2=[arr objectAtIndex:indexPath.row];
    if(dic2!=NULL)
    {
        
        NSString *string=[NSString stringWithFormat:@"%@",[dic2 objectForKey:@"picture"]];
        NSLog(@" im %@",string);
        NSString *title=[NSString stringWithFormat:@"%@",[(NSDictionary*)[dic2 objectForKey:@"from" ]objectForKey:@"name"]];
        NSString *description=[NSString stringWithFormat:@"%@",[dic2 objectForKey:@"message"]];
        feedCell.title.text=title;
        feedCell.discription.text=description;
         NSString *imgName=[NSString stringWithFormat:@"%@",[dic2 objectForKey:@"picture"]];
        [feedCell.imageView setImageWithURL:[NSURL URLWithString:imgName]];
    
//        HJManagedImageV *image=(HJManagedImageV*)[feedCell viewWithTag:5];
//        [image clear];
//        [image showLoadingWheel];
//        [image setUrl:[NSURL URLWithString:string]];
//        [[(AppDelegate*)[[UIApplication sharedApplication]delegate ]objman]manage:image];

        
    }
    
    return feedCell;
}

// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return counnt;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 100;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
