//
//  TabViewController.h
//  JamTreeMockApp
//
//  Created by Mobytz on 06/06/13.
//  Copyright (c) 2013 Mobytz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TabViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIImageView *splashImage;

@end
