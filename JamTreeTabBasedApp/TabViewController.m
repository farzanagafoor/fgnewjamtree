//
//  TabViewController.m
//  JamTreeMockApp
//
//  Created by Mobytz on 06/06/13.
//  Copyright (c) 2013 Mobytz. All rights reserved.
//

#import "TabViewController.h"
#import "MainMenuViewController.h"
#import "ContactUsViewController.h"

@interface TabViewController ()
{
    NSTimer                 *timer;
    MainMenuViewController  *objectMain;
    ContactUsViewController *objectContact;
    UITabBarController      *objectTab;
    NSMutableArray          *ControllerArray;
}
@end

@implementation TabViewController
@synthesize splashImage;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    objectTab       = [[UITabBarController alloc] init];
    objectMain      = [[MainMenuViewController alloc] initWithNibName:@"MainMenuViewController" bundle:nil];
    objectContact   = [[ContactUsViewController alloc] initWithNibName:@"ContactUsViewController" bundle:nil];
    objectMain.title = @"Main Menu";
    objectContact.title = @"Contact Us";
    ControllerArray = [[NSMutableArray alloc] initWithObjects:objectMain,objectContact, nil];
    timer           = [NSTimer scheduledTimerWithTimeInterval:1.5 target:self selector:@selector(stopSplashImage) userInfo:nil repeats:NO];
}
-(void) stopSplashImage
{
    [timer invalidate];
     timer = nil;
    [splashImage setAlpha:0];
     objectTab.viewControllers = ControllerArray;
    [self.navigationController pushViewController:objectTab animated:NO];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
