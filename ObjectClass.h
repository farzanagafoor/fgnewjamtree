//
//  ObjectClass.h
//  JamTreeTabBasedApp
//
//  Created by mobytz004 on 22/07/13.
//  Copyright (c) 2013 Mobytz. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ObjectClass : NSObject
    {
        NSNumber *catId;
        NSNumber *delete;
        NSString  *image;
        NSString  *name;
        NSString  *desc;
        
    }
    @property(nonatomic,retain) NSNumber *catId,*delete;
    @property(nonatomic,retain) NSString *image,*name,*desc;



@end
