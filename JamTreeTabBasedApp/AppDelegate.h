//
//  AppDelegate.h
//  JamTreeTabBasedApp
//
//  Created by Mobytz on 10/06/13.
//  Copyright (c) 2013 Mobytz. All rights reserved.
//
#define NOTIF_ORDER_PLACED @"NOTIF_ORDER_PLACED"
#define NOTIF_DATALOADCOMPLETE  @"NOTIF_DATALOADCOMPLETE"
#import <UIKit/UIKit.h>
#import "Category.h"
#import "WebService.h"
#import "SDWebImageManager.h"
#import "HJObjManager.h"

//#import<SDWebImage/UIImageView+WebCache.h>


@class AppDelegate,WebService;
@class FetchandParse_CategoryList;
@interface AppDelegate : UIResponder <UIApplicationDelegate, UITabBarControllerDelegate>{
  NSMutableArray *categoryList,*offerList;
    //RKRestaurant *restaurant;
    NSMutableArray *categoryDetails;
    Boolean isNetworkAvailable;
    NSMutableArray *catIdArray;
    SDWebImageManager *manager;
    
    NSMutableArray *IDArray;
    Category *catrgory;
    int count;
    int ID;
    int arrCount;
    NSMutableDictionary *categoryDictnry;
    NSMutableDictionary *menuItemsDictnry;
    NSMutableDictionary *tempDict;
    NSMutableDictionary *cachedDict;
     
}
@property(nonatomic,assign) HJObjManager *objman;
@property (nonatomic,retain)NSMutableDictionary *tempDict;
@property (nonatomic,retain)NSMutableDictionary *cachedDict ;
@property (nonatomic,retain)NSMutableDictionary *categoryDictnry;
@property (nonatomic,retain)NSMutableDictionary *menuItemsDictnry;
@property (nonatomic,retain) NSMutableArray *categoryDetails;
@property (nonatomic,retain) NSMutableArray *IDArray;

@property (retain, nonatomic)SDWebImageManager *manager;
@property (nonatomic)int count;
@property (nonatomic)int ID;
@property (nonatomic)int arrCount;
@property (retain, nonatomic)  NSMutableArray *catIdArray;

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) UITabBarController *tabBarController;
@property (strong, nonatomic) UINavigationController *navigationController;
@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;


-(UITabBarController*) getTabBarController;
- (BOOL) Detect_Network;
-(void)arrayCount:(int)count;

//-(void)initRestKit;

@end
