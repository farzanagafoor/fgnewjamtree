
//  sqliteManager.m
//  bionicPhotoManager
//
//  Created by Mobytz on 12/03/2013.
//  Copyright (c) 2013 Kiran Babu. All rights reserved.
//

#import "sqliteManager.h"
//#import "story.h"
#import "Category.h"
#import "ObjectClass.h"
@class story;
@implementation sqliteManager
-(void)createDatabase
{
	BOOL executed;
	Manage_File = [NSFileManager defaultManager];
	Sql_Path = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	Directory_docs = [Sql_Path objectAtIndex:0];
	Db_Path = [Directory_docs stringByAppendingPathComponent:@"JamTree.sqlite"];
	executed = [Manage_File fileExistsAtPath:Db_Path];
	if(executed) return;
	dbPath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"jamTree.sqlite"];
	executed = [Manage_File copyItemAtPath:dbPath toPath:Db_Path error:nil];
	//if(!executed)
		//NSAssert1(0, @"Failed to copy the database. Error: %@.", [error localizedDescription]);
}

/*
-(void)clearDatabase:(NSString*)table
{
    Sql_Path = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	Directory_docs = [Sql_Path objectAtIndex:0];
	dbPath = [Directory_docs stringByAppendingPathComponent:@"dbBiopedia1.sqlite"];
    
    if(sqlite3_open([dbPath UTF8String], &database) == SQLITE_OK)
	{
		

    NSString * tempstring= [NSString stringWithFormat:@"delete from %@",table];
    const char * sql;
    sql= [tempstring cStringUsingEncoding:NSASCIIStringEncoding];

   // NSString *query = @"delete from yourTable";
    //const char *sqlStatement = [query UTF8String];
    sqlite3_stmt *compiledStatement;
    if(sqlite3_prepare_v2(database, sql, -1, &compiledStatement, NULL) == SQLITE_OK) {
        // Loop through the results and add them to the feeds array
        while(sqlite3_step(compiledStatement) == SQLITE_ROW) {
            // Read the data from the result row
            NSLog(@"result is here");
        }
        
        // Release the compiled statement from memory
        sqlite3_finalize(compiledStatement);
    }
    }
}
 */

-(void)insertValuesIntoDbApp:(ObjectClass *)myObject
{
    Sql_Path = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	Directory_docs = [Sql_Path objectAtIndex:0];
	dbPath = [Directory_docs stringByAppendingPathComponent:@"JamTree.sqlite"];
      //  NSLog(@"%@",myObject.name);
    NSLog(@"%@",dbPath);
     NSLog(@"111%d",SQLITE_OK);
    if(sqlite3_open([dbPath UTF8String], &database) == SQLITE_OK)
	{
		 
		const char * sql;
        
        sql = "insert into category (catId,name,desc,delete,image) values (?,?,?,?,?)";
        sqlite3_stmt *selectStatement;
		
		//prepare the select statement
        
		int returnValue = sqlite3_prepare_v2(database, sql, -1, &selectStatement, NULL);
         NSLog(@"%d",SQLITE_OK);
		if(returnValue == SQLITE_OK)
		{
            
            
           sqlite3_bind_int(selectStatement, 1,[myObject.catId intValue]);
            sqlite3_bind_int(selectStatement, 2,[myObject.delete intValue]);

            sqlite3_bind_text(selectStatement,3,[myObject.name UTF8String],[myObject.name length], SQLITE_STATIC);
            sqlite3_bind_text(selectStatement,4,[myObject.desc UTF8String],[myObject.desc length], SQLITE_STATIC);
            sqlite3_bind_text(selectStatement,5,[myObject.image UTF8String],[myObject.image length], SQLITE_STATIC);
            
            
                       
            
        
            NSLog(@"111%@",myObject.name);

        
        if(sqlite3_step(selectStatement)==SQLITE_DONE)
        {
             NSLog(@"ok");
        }
            sqlite3_finalize(selectStatement);

            
          //  while(sqlite3_step(selectStatement) == SQLITE_ROW)
                // sqlite3_bind_int(selectStatement, 10, act+ rand());
                
                //loop all the rows returned by the query.  (sqlite3_step(selectStatement) == 		}
                //Release the select statement memory
            
        }
        //Close the connection.
        else
            sqlite3_close(database);
        
    }
   
    
}




-(void)editValuesIntoDbApp:(ObjectClass *)myObject
{
    Sql_Path = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	Directory_docs = [Sql_Path objectAtIndex:0];
	dbPath = [Directory_docs stringByAppendingPathComponent:@"JamTree.sqlite"];
 //int iddemo=[myObject._id intValue];
    if(sqlite3_open([dbPath UTF8String], &database) == SQLITE_OK)
	{
        
		const char * sql;
               
        sql = "update  Category set name='video2',image='mob.png' where catId=14";
        sqlite3_stmt *selectStatement;
		
		//prepare the select statement
        
		int returnValue = sqlite3_prepare_v2(database, sql, -1, &selectStatement, NULL);
        NSLog(@"%d",SQLITE_OK);
		if(returnValue == SQLITE_OK)
		{
            
            
           // sqlite3_bind_int(selectStatement, 1,[myObject._id intValue]);
            sqlite3_bind_text(selectStatement,1,[myObject.name UTF8String],[myObject.name length], SQLITE_STATIC);
            sqlite3_bind_text(selectStatement,2,[myObject.desc UTF8String],[myObject.desc length], SQLITE_STATIC);
            sqlite3_bind_text(selectStatement,3,[myObject.image UTF8String],[myObject.image length], SQLITE_STATIC);
            
            
            
            
            
            NSLog(@"111%@",myObject.name);
           
            
            
            if(sqlite3_step(selectStatement)==SQLITE_DONE)
            {
                NSLog(@"ok");
            }
            sqlite3_finalize(selectStatement);
            
            
            //  while(sqlite3_step(selectStatement) == SQLITE_ROW)
            // sqlite3_bind_int(selectStatement, 10, act+ rand());
            
            //loop all the rows returned by the query.  (sqlite3_step(selectStatement) == 		}
            //Release the select statement memory
            
        }
        //Close the connection.
        else
            sqlite3_close(database);
        
    }
    
    
}

/*

-(void)insertValuesIntoDbslidecoll:(myObjectClass*)myObject
{
    Sql_Path = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	Directory_docs = [Sql_Path objectAtIndex:0];
	dbPath = [Directory_docs stringByAppendingPathComponent:@"dbBiopedia1.sqlite"];
    
    if(sqlite3_open([dbPath UTF8String], &database) == SQLITE_OK)
	{
		
		const char * sql;
        
        sql = "insert into tbslideshowcoll (apptitle,slideshowid,slideshowtitle,slideshowdesc) values (?,?,?,?)";
        sqlite3_stmt *selectStatement;
		
		//prepare the select statement
		int returnValue = sqlite3_prepare_v2(database, sql, -1, &selectStatement, NULL);
		if(returnValue == SQLITE_OK)
		{
            
            sqlite3_bind_text(selectStatement,1,[myObject.appTitle UTF8String],[myObject.appTitle length], SQLITE_STATIC);
            sqlite3_bind_text(selectStatement,2,[myObject.slideShow_ID UTF8String],[myObject.slideShow_ID length], SQLITE_STATIC);
            sqlite3_bind_text(selectStatement,3,[myObject.slideShowTitle UTF8String],[myObject.slideShowTitle length], SQLITE_STATIC);
            sqlite3_bind_text(selectStatement,4,[myObject.slideShowDesc UTF8String],[myObject.slideShowDesc length], SQLITE_STATIC);
            while(sqlite3_step(selectStatement) == SQLITE_ROW)
                // sqlite3_bind_int(selectStatement, 10, act+ rand());
                
                //loop all the rows returned by the query.  (sqlite3_step(selectStatement) == 		}
                //Release the select statement memory
                sqlite3_finalize(selectStatement);
        }
        //Close the connection.
        else
            sqlite3_close(database);
        
    }
    
}

-(void)insertValuesIntoDbfav:(slide*)myObject
{
    Sql_Path = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	Directory_docs = [Sql_Path objectAtIndex:0];
	dbPath = [Directory_docs stringByAppendingPathComponent:@"dbBiopedia1.sqlite"];
    
    if(sqlite3_open([dbPath UTF8String], &database) == SQLITE_OK)
	{
		const char * sql;
        sql = "insert into tbFav (slidetitle,slidedesc,slidebigimage,slidethumbnail,slidelatitude,slidelongitude) values (?,?,?,?,?,?)";
        sqlite3_stmt *selectStatement;
		//prepare the select statement
		int returnValue = sqlite3_prepare_v2(database, sql, -1, &selectStatement, NULL);
		if(returnValue == SQLITE_OK)
		{
            sqlite3_bind_text(selectStatement,1,[myObject.title UTF8String],[myObject.title length], SQLITE_STATIC);
            sqlite3_bind_text(selectStatement,2,[myObject.description UTF8String],[myObject.description length], SQLITE_STATIC);
            sqlite3_bind_text(selectStatement,3,[myObject.bigimage UTF8String],[myObject.bigimage length], SQLITE_STATIC);
            sqlite3_bind_text(selectStatement,4,[myObject.thumbnail UTF8String],[myObject.thumbnail length], SQLITE_STATIC);
            sqlite3_bind_text(selectStatement,5,[myObject.latitude UTF8String],[myObject.latitude length], SQLITE_STATIC);
            sqlite3_bind_text(selectStatement,6,[myObject.longitude UTF8String],[myObject.longitude length], SQLITE_STATIC);

            while(sqlite3_step(selectStatement) == SQLITE_ROW)
                sqlite3_finalize(selectStatement);
        }
        //Close the connection.
        else
            sqlite3_close(database);
    }
    

}

-(void)deleteFavFromDB:(slide *)myObject
{
    Sql_Path = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	Directory_docs = [Sql_Path objectAtIndex:0];
	dbPath = [Directory_docs stringByAppendingPathComponent:@"dbBiopedia1.sqlite"];
    if(sqlite3_open([dbPath UTF8String], &database) == SQLITE_OK)
	{
		
		const char * sql;
        
        sql = "delete from tbFav  where slidetitle=? and slidedesc=? and slidebigimage=? and slidethumbnail=? and slidelatitude=? and slidelongitude=? ";
        sqlite3_stmt *selectStatement;
		//prepare the select statement
		int returnValue = sqlite3_prepare_v2(database, sql, -1, &selectStatement, NULL);
		if(returnValue == SQLITE_OK)
		{
            sqlite3_bind_text(selectStatement,1,[myObject.title UTF8String],[myObject.title length], SQLITE_STATIC);
            sqlite3_bind_text(selectStatement,2,[myObject.description UTF8String],[myObject.description length], SQLITE_STATIC);
            sqlite3_bind_text(selectStatement,3,[myObject.bigimage UTF8String],[myObject.bigimage length], SQLITE_STATIC);
            sqlite3_bind_text(selectStatement,4,[myObject.thumbnail UTF8String],[myObject.thumbnail length], SQLITE_STATIC);
            sqlite3_bind_text(selectStatement,5,[myObject.latitude UTF8String],[myObject.latitude length], SQLITE_STATIC);
            sqlite3_bind_text(selectStatement,6,[myObject.longitude UTF8String],[myObject.longitude length], SQLITE_STATIC);
            while(sqlite3_step(selectStatement) == SQLITE_ROW)
            {
                
            }
            //Release the select statement memory
            sqlite3_finalize(selectStatement);
        }
        //Close the connection.
        else
            sqlite3_close(database);
    }
}

-(Boolean)isFavourite:(slide*)myObject
{
    Sql_Path = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	Directory_docs = [Sql_Path objectAtIndex:0];
	dbPath = [Directory_docs stringByAppendingPathComponent:@"dbBiopedia1.sqlite"];
    int count = 0;
    if(sqlite3_open([dbPath UTF8String], &database) == SQLITE_OK)
	{
		
		const char * sql;
        
        sql = "select count(*) from tbFav  where slidetitle=? and slidedesc=? and slidebigimage=? and slidethumbnail=? and slidelatitude=? and slidelongitude=? ";
        sqlite3_stmt *selectStatement;
		//prepare the select statement
		int returnValue = sqlite3_prepare_v2(database, sql, -1, &selectStatement, NULL);
		if(returnValue == SQLITE_OK)
		{
            sqlite3_bind_text(selectStatement,1,[myObject.title UTF8String],[myObject.title length], SQLITE_STATIC);
            sqlite3_bind_text(selectStatement,2,[myObject.description UTF8String],[myObject.description length], SQLITE_STATIC);
            sqlite3_bind_text(selectStatement,3,[myObject.bigimage UTF8String],[myObject.bigimage length], SQLITE_STATIC);
            sqlite3_bind_text(selectStatement,4,[myObject.thumbnail UTF8String],[myObject.thumbnail length], SQLITE_STATIC);
            sqlite3_bind_text(selectStatement,5,[myObject.latitude UTF8String],[myObject.latitude length], SQLITE_STATIC);
            sqlite3_bind_text(selectStatement,6,[myObject.longitude UTF8String],[myObject.longitude length], SQLITE_STATIC);
            
            while(sqlite3_step(selectStatement) == SQLITE_ROW)
            {
                count = sqlite3_column_int(selectStatement, 0);
            }
            //Release the select statement memory
            sqlite3_finalize(selectStatement);
        }
        //Close the connection.
        else
            sqlite3_close(database);
    }
    if (count > 0)
    {
        return TRUE;
    }
    else
    {
        return FALSE;
    }
}

-(void)insertValuesIntoDbslides:(myObjectClass*)myObject
{
    Sql_Path = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	Directory_docs = [Sql_Path objectAtIndex:0];
	dbPath = [Directory_docs stringByAppendingPathComponent:@"dbBiopedia1.sqlite"];
    
    if(sqlite3_open([dbPath UTF8String], &database) == SQLITE_OK)
	{
		
		const char * sql;
        
        sql = "insert into tbslides (slideshowid,slidetitle,slidedesc,slidebigimage,slidethumbnail,slidelatitude,slidelongitude,slideisdeleted,slidephotographer,slidekeywords,slidecopyright,slideloctags,slidenumber) values (?,?,?,?,?,?,?,?,?,?,?,?,?)";
        
        
        sqlite3_stmt *selectStatement;
		
		//prepare the select statement
		int returnValue = sqlite3_prepare_v2(database, sql, -1, &selectStatement, NULL);
		if(returnValue == SQLITE_OK)
		{
            
            sqlite3_bind_text(selectStatement,1,[myObject.slideShow_ID UTF8String],[myObject.slideShow_ID length], SQLITE_STATIC);
            sqlite3_bind_text(selectStatement,2,[myObject.slideTitle UTF8String],[myObject.slideTitle length], SQLITE_STATIC);
            sqlite3_bind_text(selectStatement,3,[myObject.slideDesc UTF8String],[myObject.slideDesc length], SQLITE_STATIC);
            sqlite3_bind_text(selectStatement,4,[myObject.slideBigImage UTF8String],[myObject.slideBigImage length], SQLITE_STATIC);
            sqlite3_bind_text(selectStatement,5,[myObject.slideThumbnail UTF8String],[myObject.slideThumbnail length], SQLITE_STATIC);
            sqlite3_bind_text(selectStatement,6,[myObject.slidelatitude UTF8String],[myObject.slidelatitude length], SQLITE_STATIC);
            sqlite3_bind_text(selectStatement,7,[myObject.slidelongitude UTF8String],[myObject.slidelongitude length], SQLITE_STATIC);
            sqlite3_bind_text(selectStatement,8,[myObject.isSlideDeleted UTF8String],[myObject.isSlideDeleted length], SQLITE_STATIC);
            sqlite3_bind_text(selectStatement,9,[myObject.slidePhotographer UTF8String],[myObject.slidePhotographer length], SQLITE_STATIC);
            sqlite3_bind_text(selectStatement,10,[myObject.slideKeyword UTF8String],[myObject.slideKeyword length], SQLITE_STATIC);
            sqlite3_bind_text(selectStatement,11,[myObject.slideCopyright UTF8String],[myObject.slideCopyright length], SQLITE_STATIC);
            sqlite3_bind_text(selectStatement,12,[myObject.slideLocTags UTF8String],[myObject.slideLocTags length], SQLITE_STATIC);
            sqlite3_bind_int(selectStatement, 13, [myObject.slideNumber intValue]);

            while(sqlite3_step(selectStatement) == SQLITE_ROW)
            
                // sqlite3_bind_int(selectStatement, 10, act+ rand());
                
                //loop all the rows returned by the query.  (sqlite3_step(selectStatement) == 		}
                //Release the select statement memory
                sqlite3_finalize(selectStatement);
        }
        //Close the connection.
        else
            sqlite3_close(database);
        
    }
    
    
}


-(void)insertValuesIntoDbstorycoll:(myObjectClass*)myObject
{
    Sql_Path = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	Directory_docs = [Sql_Path objectAtIndex:0];
	dbPath = [Directory_docs stringByAppendingPathComponent:@"dbBiopedia1.sqlite"];
    
    if(sqlite3_open([dbPath UTF8String], &database) == SQLITE_OK)
	{
		
		const char * sql;
        
        sql = "insert into tbstorycoll (apptitle,storiesid,storiestitle,storiesdesc,storiesisdeleted) values (?,?,?,?,?)";
        sqlite3_stmt *selectStatement;
		
		//prepare the select statement
		int returnValue = sqlite3_prepare_v2(database, sql, -1, &selectStatement, NULL);
		if(returnValue == SQLITE_OK)
		{
            
            sqlite3_bind_text(selectStatement,1,[myObject.appTitle UTF8String],[myObject.appTitle length], SQLITE_STATIC);
            sqlite3_bind_text(selectStatement,2,[myObject.storiesId UTF8String],[myObject.storiesId length], SQLITE_STATIC);
            sqlite3_bind_text(selectStatement,3,[myObject.storiesTitle UTF8String],[myObject.storiesTitle length], SQLITE_STATIC);
            sqlite3_bind_text(selectStatement,4,[myObject.storiesDesc UTF8String],[myObject.storiesDesc length], SQLITE_STATIC);
            sqlite3_bind_text(selectStatement,5,[myObject.isStoriesDeleted UTF8String],[myObject.isStoriesDeleted length], SQLITE_STATIC);
            while(sqlite3_step(selectStatement) == SQLITE_ROW)
                // sqlite3_bind_int(selectStatement, 10, act+ rand());
                
                //loop all the rows returned by the query.  (sqlite3_step(selectStatement) == 		}
                //Release the select statement memory
                sqlite3_finalize(selectStatement);
        }
        //Close the connection.
        else
            sqlite3_close(database);
        
    }
    
}

-(void)insertValuesIntoDbstory:(myObjectClass*)myObject
{
    Sql_Path = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	Directory_docs = [Sql_Path objectAtIndex:0];
	dbPath = [Directory_docs stringByAppendingPathComponent:@"dbBiopedia1.sqlite"];
    
    if(sqlite3_open([dbPath UTF8String], &database) == SQLITE_OK)
	{
		
		const char * sql;
        
        sql = "insert into tbstory (storiesid,storytitle,storydesc,storybigimage,storythumbnail,storylink,storyphotographer,storylatitude,storylongitude,storyarea,storytype,storyisdeleted,storykeywords,storyloctags,storycopyright,storynumber) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        sqlite3_stmt *selectStatement;
		
		//prepare the select statement
		int returnValue = sqlite3_prepare_v2(database, sql, -1, &selectStatement, NULL);
		if(returnValue == SQLITE_OK)
		{
            
            sqlite3_bind_text(selectStatement,1,[myObject.storiesId UTF8String],[myObject.slideShow_ID length], SQLITE_STATIC);
            sqlite3_bind_text(selectStatement,2,[myObject.storyTitle UTF8String],[myObject.storyTitle length], SQLITE_STATIC);
            sqlite3_bind_text(selectStatement,3,[myObject.storyDesc UTF8String],[myObject.storyDesc length], SQLITE_STATIC);
            sqlite3_bind_text(selectStatement,4,[myObject.storyBigImage UTF8String],[myObject.storyBigImage length], SQLITE_STATIC);
            sqlite3_bind_text(selectStatement,5,[myObject.storyThumbnail UTF8String],[myObject.storyThumbnail length], SQLITE_STATIC);
            sqlite3_bind_text(selectStatement,6,[myObject.storyLink UTF8String],[myObject.storyLink length], SQLITE_STATIC);
            
            sqlite3_bind_text(selectStatement,7,[myObject.storyPhotographer UTF8String],[myObject.storyPhotographer length], SQLITE_STATIC);
            sqlite3_bind_text(selectStatement,8,[myObject.storyLatitude UTF8String],[myObject.storyLatitude length], SQLITE_STATIC);
            sqlite3_bind_text(selectStatement,9,[myObject.storyLongitude UTF8String],[myObject.storyLongitude length], SQLITE_STATIC);
            sqlite3_bind_text(selectStatement,10,[myObject.storyArea UTF8String],[myObject.storyArea length], SQLITE_STATIC);
            sqlite3_bind_text(selectStatement,11,[myObject.storyType UTF8String],[myObject.storyType length], SQLITE_STATIC);
            sqlite3_bind_text(selectStatement,12,[myObject.isStoryDeleted UTF8String],[myObject.isStoryDeleted length], SQLITE_STATIC);
            sqlite3_bind_text(selectStatement,13,[myObject.storyKeyword UTF8String],[myObject.storyKeyword length], SQLITE_STATIC);
            sqlite3_bind_text(selectStatement,14,[myObject.storyLocTags UTF8String],[myObject.storyLocTags length], SQLITE_STATIC);
            sqlite3_bind_text(selectStatement,15,[myObject.storyCopyright UTF8String],[myObject.storyCopyright length], SQLITE_STATIC);
            sqlite3_bind_int(selectStatement, 16, [myObject.storyNumber intValue]);
            while(sqlite3_step(selectStatement) == SQLITE_ROW)
                // sqlite3_bind_int(selectStatement, 10, act+ rand());
                
                //loop all the rows returned by the query.  (sqlite3_step(selectStatement) == 		}
                //Release the select statement memory
                sqlite3_finalize(selectStatement);
        }
        //Close the connection.
        else
            sqlite3_close(database);
        
    }
    
}

-(NSMutableArray*)getappdetails;
{
    Sql_Path = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	Directory_docs = [Sql_Path objectAtIndex:0];
	dbPath = [Directory_docs stringByAppendingPathComponent:@"dbBiopedia1.sqlite"];
    NSMutableArray *arr=[[NSMutableArray alloc]init];
    // NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
    if(sqlite3_open([dbPath UTF8String], &database) == SQLITE_OK)
	{
        //        NSString *title = [_slide valueForKey:@"title"];
        //        myObject.slideTitle=title;
        //        NSString *desc = [_slide valueForKey:@"description"];
        //        myObject.slideDesc=desc;
        //        NSString *bigImage = [_slide valueForKey:@"bigimage"];
        //        myObject.slideBigImage=bigImage;
        //        NSString *thumbnail = [_slide valueForKey:@"thumbnail"];
        //        myObject.slideThumbnail=thumbnail;
        //        [sqMan insertValuesIntoDbslides:myObject];
        
		
		const char * sql;
        sql="select * from tbAppdet";
        sqlite3_stmt *compiledStatement;
        
        if (sqlite3_prepare_v2(database, sql, -1, &compiledStatement, NULL) == SQLITE_OK) {
            
            //loop through the results and add them to the feeds array
            while (sqlite3_step(compiledStatement) == SQLITE_ROW) {
                NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
                
                NSString *aAptitle= [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 0)];
                [dict setObject:aAptitle forKey:@"title"];
                
                
                NSString *aDesc = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 1)];
                [dict setObject:aDesc forKey:@"description"];
                [arr addObject:dict];
            }
        }
    }
    
    
    return arr;

}

-(NSMutableArray*)getfavorites
{
    Sql_Path = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	Directory_docs = [Sql_Path objectAtIndex:0];
	dbPath = [Directory_docs stringByAppendingPathComponent:@"dbBiopedia1.sqlite"];
    NSMutableArray *arr=[[NSMutableArray alloc]init];
    // NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
    if(sqlite3_open([dbPath UTF8String], &database) == SQLITE_OK)
	{
        
		NSString * tempstring= [NSString stringWithFormat:@"select * from tbFav "];
        const char * sql;
        sql= [tempstring cStringUsingEncoding:NSASCIIStringEncoding];
		
        sqlite3_stmt *compiledStatement;
        
        if (sqlite3_prepare_v2(database, sql, -1, &compiledStatement, NULL) == SQLITE_OK) {
            
            //loop through the results and add them to the feeds array
            while (sqlite3_step(compiledStatement) == SQLITE_ROW) {
                slide *slide1=[[slide alloc]init];
                
                NSString *aTitle = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 0)];
                // [dict setObject:aTitle forKey:@"title"];
                slide1.title=aTitle;
                
                NSString *aDesc = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 1)];
                // [dict setObject:aDesc forKey:@"description"];
                slide1.description=aDesc;
                
                NSString *aBigimage = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 2)];
                //[dict setObject:aBigimage forKey:@"bigimage"];
                slide1.bigimage=aBigimage;
                
                NSString *aThumbnail = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 3)];
                //[dict setObject:aThumbnail forKey:@"thumbnail"];
                slide1.thumbnail=aThumbnail;
                NSString *alatitude = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 4)];
                //[dict setObject:aThumbnail forKey:@"thumbnail"];
                slide1.latitude=alatitude;
                NSString *alongitude = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 5)];
                //[dict setObject:aThumbnail forKey:@"thumbnail"];
                slide1.longitude=alongitude;
                
                [arr addObject:slide1];
                [slide1 release];
                
            }
        }
    }
    
    
    return arr;

}

-(NSMutableArray*)getslidecollectiowithapptitle:(NSString*)apptitle
{
    Sql_Path = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	Directory_docs = [Sql_Path objectAtIndex:0];
	dbPath = [Directory_docs stringByAppendingPathComponent:@"dbBiopedia1.sqlite"];
    NSMutableArray *arr=[[NSMutableArray alloc]init];
    // NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
    if(sqlite3_open([dbPath UTF8String], &database) == SQLITE_OK)
	{
//        NSString *slideshow_ID = [slides valueForKey:@"slideshow_ID"];
//        myObject.slideShow_ID=slideshow_ID;
//        NSString *title = [slides valueForKey:@"slidwshow_Title"];
//        myObject.slideShowTitle=title;
//        NSString *desc = [slides valueForKey:@"slideshow_description"];
        NSString * tempstring= [NSString stringWithFormat:@"select * from tbslideshowcoll where apptitle='%@'",apptitle];
        const char * sql;
        sql= [tempstring cStringUsingEncoding:NSASCIIStringEncoding];

		
		//const char * sql;
        //sql="select * from tbslideshowcoll where apptitle=";
        sqlite3_stmt *compiledStatement;
        
        if (sqlite3_prepare_v2(database, sql, -1, &compiledStatement, NULL) == SQLITE_OK) {
            
            //loop through the results and add them to the feeds array
            while (sqlite3_step(compiledStatement) == SQLITE_ROW) {
                NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
                
                NSString *aShowid= [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 1)];
                [dict setObject:aShowid forKey:@"slideshowid"];
                
                
                NSString *aShowtitle = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 2)];
                [dict setObject:aShowtitle forKey:@"slideshowtitle"];
                
                NSString *aShowdesc = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 3)];
                [dict setObject:aShowdesc forKey:@"slideshowdescription"];
                
                [arr addObject:dict];
            }
        }
    }
    
    
    return arr;

}



-(NSMutableArray*)getslideswithslidecollection:(NSString *)slidecoll_Id
{
    Sql_Path = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	Directory_docs = [Sql_Path objectAtIndex:0];
	dbPath = [Directory_docs stringByAppendingPathComponent:@"dbBiopedia1.sqlite"];
    NSMutableArray *arr=[[NSMutableArray alloc]init];
    // NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
    if(sqlite3_open([dbPath UTF8String], &database) == SQLITE_OK)
	{
        
		NSString * tempstring= [NSString stringWithFormat:@"select * from tbslides where slideshowid='%@'",slidecoll_Id];
        const char * sql;
        sql= [tempstring cStringUsingEncoding:NSASCIIStringEncoding];
		
        sqlite3_stmt *compiledStatement;
        
        if (sqlite3_prepare_v2(database, sql, -1, &compiledStatement, NULL) == SQLITE_OK) {
            
            //loop through the results and add them to the feeds array
            while (sqlite3_step(compiledStatement) == SQLITE_ROW) {
                slide *slide1=[[slide alloc]init];
                 
                NSString *aTitle = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 1)];
               // [dict setObject:aTitle forKey:@"title"];
                slide1.title=aTitle;
                
                NSString *aDesc = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 2)];
               // [dict setObject:aDesc forKey:@"description"];
                slide1.description=aDesc;

                NSString *aBigimage = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 3)];
                //[dict setObject:aBigimage forKey:@"bigimage"];
                slide1.bigimage=aBigimage;

                NSString *aThumbnail = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 4)];
                //[dict setObject:aThumbnail forKey:@"thumbnail"];
                slide1.thumbnail=aThumbnail;
                NSString *alatitude = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 7)];
                //[dict setObject:aThumbnail forKey:@"thumbnail"];
                slide1.latitude=alatitude;
                NSString *alongitude = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 8)];
                //[dict setObject:aThumbnail forKey:@"thumbnail"];
                slide1.longitude=alongitude;
                
                slide1.slidePhotographer = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 9)];
                slide1.slideKeyword = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 10)];
                slide1.slideCopyright = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 11)];
                slide1.slideLocTags = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 12)];
                slide1.slideNumber = sqlite3_column_int(compiledStatement, 13);
                [arr addObject:slide1];
                [slide1 release];
                
            }
        }
    }
    
    
    return arr;
    
}

-(NSMutableArray*)getstoriescollectiowithapptitle:(NSString*)apptitle
{
//    NSString *storyid = [stories valueForKey:@"story_ID"];
//    myObject.storiesId=storyid;
//    NSString *title = [stories valueForKey:@"storytitle"];
//    myObject.storiesTitle=title;
//    NSString *desc = [stories valueForKey:@"storydescription"];
//    myObject.storiesDesc=desc;
    Sql_Path = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	Directory_docs = [Sql_Path objectAtIndex:0];
	dbPath = [Directory_docs stringByAppendingPathComponent:@"dbBiopedia1.sqlite"];
    NSMutableArray *arr=[[NSMutableArray alloc]init];
    // NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
    if(sqlite3_open([dbPath UTF8String], &database) == SQLITE_OK)
	{
        //        NSString *slideshow_ID = [slides valueForKey:@"slideshow_ID"];
        //        myObject.slideShow_ID=slideshow_ID;
        //        NSString *title = [slides valueForKey:@"slidwshow_Title"];
        //        myObject.slideShowTitle=title;
        //        NSString *desc = [slides valueForKey:@"slideshow_description"];
        NSString * tempstring= [NSString stringWithFormat:@"select * from tbstorycoll where apptitle='%@' and storiesisdeleted='False'",apptitle];
        const char * sql;
        sql= [tempstring cStringUsingEncoding:NSASCIIStringEncoding];
        
		
		//const char * sql;
        //sql="select * from tbslideshowcoll where apptitle=";
        sqlite3_stmt *compiledStatement;
        
        if (sqlite3_prepare_v2(database, sql, -1, &compiledStatement, NULL) == SQLITE_OK) {
            
            //loop through the results and add them to the feeds array
            while (sqlite3_step(compiledStatement) == SQLITE_ROW) {
                NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
                
                NSString *aStoriesid= [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 1)];
                [dict setObject:aStoriesid forKey:@"storiesid"];
                
                
                NSString *aStoriestitle = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 2)];
                [dict setObject:aStoriestitle forKey:@"storiestitle"];
                
                NSString *aStoriesdesc = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 3)];
                [dict setObject:aStoriesdesc forKey:@"storiesdescription"];
                
                [arr addObject:dict];
            }
        }
    }
    
    
    return arr;


    
}


-(NSMutableArray*)getstorieswithstoriescollection:(NSString *)stories_Id

{
   // story *k=[[story alloc]init];
    Sql_Path = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	Directory_docs = [Sql_Path objectAtIndex:0];
	dbPath = [Directory_docs stringByAppendingPathComponent:@"dbBiopedia1.sqlite"];
    NSMutableArray *arr=[[NSMutableArray alloc]init];
    // NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
    if(sqlite3_open([dbPath UTF8String], &database) == SQLITE_OK)
	{
        //        NSString *title = [_slide valueForKey:@"title"];
        //        myObject.slideTitle=title;
        //        NSString *desc = [_slide valueForKey:@"description"];
        //        myObject.slideDesc=desc;
        //        NSString *bigImage = [_slide valueForKey:@"bigimage"];
        //        myObject.slideBigImage=bigImage;
        //        NSString *thumbnail = [_slide valueForKey:@"thumbnail"];
        //        myObject.slideThumbnail=thumbnail;
        //        [sqMan insertValuesIntoDbslides:myObject];
        
		NSString * tempstring= [NSString stringWithFormat:@"select * from tbstory where storiesid='%@' and storyisdeleted='False'",stories_Id];
        const char * sql;
        sql= [tempstring cStringUsingEncoding:NSASCIIStringEncoding];
		//const char * sql;
       // sql="select * from tbstory where storiesid=";
        sqlite3_stmt *compiledStatement;
        
        if (sqlite3_prepare_v2(database, sql, -1, &compiledStatement, NULL) == SQLITE_OK) {
            
            //loop through the results and add them to the feeds array
            while (sqlite3_step(compiledStatement) == SQLITE_ROW) {
                story *k=[[story alloc]init];

                
                
                NSString *aStoriesid = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 0)];
               
                k.storiesid=aStoriesid;
                
                NSString *aTitle = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 1)];
                
                k.title=aTitle;
                
                NSString *aDesc = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 2)];
                
                k.description=aDesc;
                
                NSString *aBigimage = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 3)];
                
                k.bigimage=aBigimage;
                
                NSString *aThumbnail = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 4)];
                
                k.thumbnail=aThumbnail;
              //  [arr addObject:dict];
                NSString *aLink = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 5)];
                
                k.link=aLink;
                //[arr addObject:dict];
                NSString *aPhotographer = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 6)];
                
                k.photographer=aPhotographer;
                //[arr addObject:dict];
                NSString *aLatitude = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 7)];
                
                k.latitude=aLatitude;
               // [arr addObject:dict];
                NSString *aLongitude = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 8)];
                
                k.longitude=aLongitude;
               // [arr addObject:dict];
                NSString *aArea = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 9)];
                
                k.area=aArea;
                //[arr addObject:dict];
                NSString *aType = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 10)];
                
                k.type=aType;
                k.storyLocTags = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 14)];
                k.storyKeyword = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 13)];
                k.storyCopyright = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 15)];
                k.storyNumber = sqlite3_column_int(compiledStatement, 16);
                [arr addObject:k];
                [k release];
                
            }
        }
    }
    
    
    return arr;
    

}
 */

@end


