//
//  ListTableViewController.h
//  JamTreeMockApp
//
//  Created by Mobytz on 10/06/13.
//  Copyright (c) 2013 Mobytz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ListTableViewController : UIViewController

- (IBAction)backButtonPressed:(id)sender;
@end
