//
//  MainMenuViewController.h
//  JamTreeMockApp
//
//  Created by Mobytz on 06/06/13.
//  Copyright (c) 2013 Mobytz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "Global.h"
#import "MBProgressHUD.h"
#import "SDWebImageManager.h"
#import "ObjectClass.h"

@class menuView;
@interface MainMenuViewController : UIViewController<SDWebImageManagerDelegate,NSFetchedResultsControllerDelegate>
{
    menuView       *objectMenu;
    NSMutableArray        *mainMenuArray;
    AppDelegate *deleg;
    
    NSMutableArray *array;
    NSMutableArray *arr;
    MBProgressHUD *HUD;
    menuView *page1Object;
    ObjectClass *obj;
   
    
}

@property (nonatomic, retain)NSMutableArray *arr;
@property (strong, nonatomic) IBOutlet UIView *splashView;
@property (retain, nonatomic) IBOutlet UIView *animationView;
@property (weak, nonatomic) IBOutlet UIScrollView *mainScrollView;
@property (nonatomic, retain) menuView *objectMenu;
@property(nonatomic,retain) ObjectClass *obj;
 @property(nonatomic,retain)MBProgressHUD *HUD;
-(void)doParsing:(NSNotification *)dict;
-(void) menuButtonSelected:(int) value;
-(void)getCategoryData;
-(void)getMenuItemsData;
-(void)saveCategoryData;
-(void)saveMenuItemsData;
@end
