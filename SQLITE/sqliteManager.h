//
//  sqliteManager.h
//  bionicPhotoManager
//
//  Created by Mobytz on 12/03/2013.
//  Copyright (c) 2013 Kiran Babu. All rights reserved.
//

#import <Foundation/Foundation.h>
//#import "ObjectClass.h"
#import <sqlite3.h>
//#import "story.h"
//#import "slide.h"

@interface sqliteManager : NSObject{
    NSError *error;
    NSFileManager *Manage_File ;
    NSArray *Sql_Path;
    NSString *Directory_docs;
    NSString *Db_Path;
    NSString *dbPath;
    sqlite3	*database;
    
}
//-(void)createDatabase;
//-(void)insertValuesIntoDbApp:(ObjectClass*)myObject;
//-(void)editValuesIntoDbApp:(ObjectClass*)myObject;
//-(void)insertValuesIntoDbslidecoll:(myObjectClass*)myObject;
//-(void)insertValuesIntoDbslides:(myObjectClass*)myObject;
//-(void)insertValuesIntoDbstorycoll:(myObjectClass*)myObject;
//-(void)insertValuesIntoDbstory:(myObjectClass*)myObject;
//-(void)insertValuesIntoDbfav:(slide*)myObject;

-(void)clearDatabase:(NSString*)table;
//-(void)deleteFavFromDB:(slide*)slide;
-(NSMutableArray*)getappdetails;
-(NSMutableArray*)getslidecollectiowithapptitle:(NSString*)apptitle;
-(NSMutableArray*)getstoriescollectiowithapptitle:(NSString*)apptitle;
-(NSMutableArray*)getslideswithslidecollection:(NSString*)slidecoll_Id;
-(NSMutableArray*)getstorieswithstoriescollection:(NSString*)stories_Id;
-(NSMutableArray*)getfavorites;
//-(Boolean)isFavourite:(slide*)myObject;
@end
