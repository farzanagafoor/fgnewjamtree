//
//  MenuItems.m
//  JamTreeTabBasedApp
//
//  Created by mobytz004 on 25/07/13.
//  Copyright (c) 2013 Mobytz. All rights reserved.
//

#import "MenuItems.h"


@implementation MenuItems

@dynamic catId;
@dynamic deleted;
@dynamic desc;
@dynamic image;
@dynamic itemId;
@dynamic name;
@dynamic price;

@end
