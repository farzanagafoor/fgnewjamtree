//
//  MainMenuSub1ViewController.m
//  JamTreeMockApp
//
//  Created by Mobytz on 07/06/13.
//  Copyright (c) 2013 Mobytz. All rights reserved.
//
#define beginOffset 25
#define RowHeight 50
#import "MainMenuSub1ViewController.h"
#import "MainMenuViewController.h"
#import "ListTableViewController.h"
#import "menuView.h"

@interface MainMenuSub1ViewController ()
{
    MainMenuViewController  *objectMain;
    ListTableViewController *objectList;
   
    AppDelegate             *objectApp;
    int i;
    
}

@end

@implementation MainMenuSub1ViewController
@synthesize     objectMenu;
@synthesize     sub1ScrollView,count,value1,n;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    deleg = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    objectApp                                = [[AppDelegate alloc] init];
    
    menuView *pageObject                     = [[menuView alloc] initWithFrame:self.view.frame];
    objectMenu                               = pageObject;
    objectMenu.objectSub1                    = self;
    
    objectMain = [[MainMenuViewController alloc] initWithNibName:@"MainMenuViewController" bundle:nil];
    objectList = [[ListTableViewController alloc] initWithNibName:@"ListTableViewController" bundle:nil];
        
}
-(void)fetch
{
    AppDelegate *deleg = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"MenuItems" inManagedObjectContext:deleg.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"catId==%@", deleg.ID];
    [fetchRequest setPredicate:predicate];
    
    NSError *error = nil;
    NSArray *fetchedObjects = [deleg.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    if (fetchedObjects == nil) {
        NSLog(@"vjjh");
    }
    deleg.cachedDict =fetchedObjects;
    
}

-(void) viewWillAppear:(BOOL)animated
{
     [self fetch];
    NSMutableArray *array=[deleg.cachedDict valueForKey:@"image"];
    [super viewWillAppear:YES];
    [self.sub1ScrollView setContentOffset:CGPointMake(0, 25)];

        for (menuView *menu in objectMenu.subviews)
    {
        [menu removeFromSuperview];
    }
    for (UIScrollView *scroll in self.sub1ScrollView.subviews)
    {
        [scroll removeFromSuperview];
    }
    
    for (int i=0; i<[array count]/7; i++) {
        
    
        [self performSelector:@selector(menuAppear:) withObject:nil afterDelay:.25f*(i+3)];
    }
    [self.sub1ScrollView setContentSize:CGSizeMake(320, (10)*([array count])+1)];
    
    [sub1ScrollView setContentOffset:CGPointMake(0, 0)];

    
}


- (void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView{
    
    NSLog(@"haii");
    
    
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
 
//    int k=(int)sub1ScrollView.contentOffset.y;
//   if (sub1ScrollView.contentOffset.y==beginOffset)
//    {
//        }
//    else
//{
//      
//   
//     
//     NSMutableArray *array=[deleg.cachedDict valueForKey:@"image"];
//    for (int t=0; t<array.count/7; t++) {
//
//        if(k/90==t){
//           n=k;
//            [UIView beginAnimations:@"start" context:nil];
//            [UIView setAnimationDuration:.5];
//        UIImageView *imgView1 = [sub1ScrollView.subviews objectAtIndex:t+2];
//        imgView1.alpha -= .3;
//        UIView *view1 = [sub1ScrollView.subviews objectAtIndex:t];
//        view1.alpha -= .3;
//        
//        UILabel *lbl1 = [sub1ScrollView.subviews objectAtIndex:t+1];
//        lbl1.alpha -= .3;
//            [UIView commitAnimations];
//        }
//        if(k>n)//((k/100==array.count/7)-2)
//        {
//            
//            for (int t=k/50; t>0; t--){
//          
//            
//                if(k/30==t){
//                [UIView beginAnimations:@"start" context:nil];
//                [UIView setAnimationDuration:.5];
//                UIImageView *imgView1 = [sub1ScrollView.subviews objectAtIndex:t+1];
//                [imgView1 setAlpha:.8];
//
//            UIView *view1 = [sub1ScrollView.subviews objectAtIndex:t-1];
//            
//                
//                [view1 setAlpha:.5];
//            
//            UILabel *lbl1 = [sub1ScrollView.subviews objectAtIndex:t];
//                [lbl1 setAlpha:.3];
//            [UIView commitAnimations];
//        }
//            }
//    }
//       
//    }
//
//    }
//    
    }



 
-(void) menuAppear:(int) value
{
    [objectMenu subMenuAnimating:value1];

    [self.sub1ScrollView addSubview:objectMenu.sView];
    [self.sub1ScrollView addSubview:objectMenu.sLabel];
    [self.sub1ScrollView addSubview:objectMenu.sImgView];
        }
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)backButtonPressed:(id)sender
{
    for (menuView *menu in objectMenu.subviews)
    {
        [menu removeFromSuperview];
    }
    for (UIScrollView *scroll in self.sub1ScrollView.subviews)
    {
        [scroll removeFromSuperview];
    }
    [self.navigationController popViewControllerAnimated:YES];
}
-(void) whenMenuButtonSelected:(int)value
{
    //int tagValue = (long int)[sender tag];
//    switch (value) {
//        case 0:
//            [self.navigationController pushViewController:objectList animated:YES];
//    
//            break;
//        case 1:
//            [self.navigationController pushViewController:objectList1 animated:YES];
//            break;
//        case 2:
//            [self.navigationController pushViewController:objectList2 animated:YES];
//            break;
//        case 3:
//            [self.navigationController pushViewController:objectList3 animated:YES];
//            break;
//            
//        
//    }
}
@end
