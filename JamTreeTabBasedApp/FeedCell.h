//
//  FeedCell.h
//  JamTreeTabBasedApp
//
//  Created by mobytz004 on 11/11/13.
//  Copyright (c) 2013 Mobytz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FeedCell : UITableViewCell
@property(nonatomic,retain) IBOutlet UILabel *title;
@property (strong, nonatomic) IBOutletCollection(UIImageView) NSArray *imgV;
@property (strong, nonatomic) IBOutlet UITextView *discription;
@end
