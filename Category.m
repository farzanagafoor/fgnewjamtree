//
//  Category.m
//  JamTreeTabBasedApp
//
//  Created by mobytz004 on 22/07/13.
//  Copyright (c) 2013 Mobytz. All rights reserved.
//

#import "Category.h"


@implementation Category

@dynamic catItem;
@dynamic deleted;
@dynamic desc;
@dynamic image;
@dynamic name;

@end
