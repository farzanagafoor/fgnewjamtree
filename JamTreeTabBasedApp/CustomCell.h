//
//  CustomCell.h
//  JamTreeMockApp
//
//  Created by Mobytz on 10/06/13.
//  Copyright (c) 2013 Mobytz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *cellImage;
@property (weak, nonatomic) IBOutlet UILabel *cellLabel;
@property (weak, nonatomic) IBOutlet UITextView *cellTextView;

@end
