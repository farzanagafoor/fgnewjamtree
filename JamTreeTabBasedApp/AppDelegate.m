//
//  AppDelegate.m
//  JamTreeTabBasedApp
//
//  Created by Mobytz on 10/06/13.
//  Copyright (c) 2013 Mobytz. All rights reserved.
//

#import "AppDelegate.h"
#import "WebService.h"
#import "MainMenuViewController.h"
#import "ContactUsViewController.h"
#import "TwitterViewController.h"
#import "FacebookViewController.h"
#import "sqliteManager.h"
#import "ObjectClass.h"
#import "HJObjManager.h"
//NSString *const FBSessionStateChangedNotification =
//@"com.mobytz.JamtreeTabBasedApp:FBSessionStateChangedNotification";


//#import "RKOffers.h"
#import "Global.h"

//#import "RKFetchOffers.h"
//#import "FetchRestaurants.h"

@implementation AppDelegate
@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator,categoryDictnry,menuItemsDictnry,tempDict,cachedDict;


@synthesize navigationController,catIdArray,manager,categoryDetails,IDArray,count,arrCount,ID,objman;

//@synthesize restaurant

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [[UIApplication sharedApplication]
	 registerForRemoteNotificationTypes:(UIRemoteNotificationTypeBadge |
										 UIRemoteNotificationTypeSound |
										 UIRemoteNotificationTypeAlert)];
//    FetchMenu *menu = [[FetchMenu alloc] init];
//    [menu fetchMenu];


    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    UIViewController *viewController1, *viewController2, *viewController3, *viewController4;
        viewController1            = [[MainMenuViewController alloc] initWithNibName:@"MainMenuViewController" bundle:nil];
        viewController2            = [[TwitterViewController alloc] initWithNibName:@"TwitterViewController" bundle:nil];
        viewController3            = [[FacebookViewController alloc] initWithNibName:@"FacebookViewController" bundle:nil];
        viewController4            = [[ContactUsViewController alloc] initWithNibName:@"ContactUsViewController" bundle:nil];
    UINavigationController *nav1, *nav2, *nav3, *nav4;
    nav1                           = [[UINavigationController alloc] initWithRootViewController:viewController1];
    nav2                           = [[UINavigationController alloc] initWithRootViewController:viewController2];
    nav3                           = [[UINavigationController alloc] initWithRootViewController:viewController3];
    nav4                           = [[UINavigationController alloc] initWithRootViewController:viewController4];
    [nav1 setNavigationBarHidden:YES];
    [nav2 setNavigationBarHidden:YES];
    [nav3 setNavigationBarHidden:YES];
    [nav4 setNavigationBarHidden:YES];
    viewController1.title          = @"Main Menu";
    viewController2.title          = @"Twitter";
    viewController3.title          = @"Facebook";
    viewController4.title          = @"Contact Us";
    
    viewController1.tabBarItem.image = [UIImage imageNamed:@"Home.png"];
    viewController2.tabBarItem.image = [UIImage imageNamed:@"bird.png"];
    viewController3.tabBarItem.image = [UIImage imageNamed:@"facebook.png"];
    viewController4.tabBarItem.image = [UIImage imageNamed:@"phone.png"];
    
    self.tabBarController          = [[UITabBarController alloc] init];
    self.tabBarController.delegate = self;
    self.tabBarController.viewControllers = @[nav1, nav2, nav3, nav4];
//self.navigationController      = [[UINavigationController alloc] initWithRootViewController:self.tabBarController];
    self.window.rootViewController = self.tabBarController;
    
    [self.navigationController setNavigationBarHidden:YES];
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    //sqliteManager *sqlmngr=[[sqliteManager alloc] init];
    //[sqlmngr createDatabase];


           [self.window makeKeyAndVisible];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

- (void)applicationDidFinishLaunching:(UIApplication *)application
    
    {
        
        // Override point for customization after application launch.
        
        
        
        }




- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

/*
// Optional UITabBarControllerDelegate method.
- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController
{
}
*/

/*
// Optional UITabBarControllerDelegate method.
- (void)tabBarController:(UITabBarController *)tabBarController didEndCustomizingViewControllers:(NSArray *)viewControllers changed:(BOOL)changed
{
}
*/
//-(void)initRestKit
//{
   // RKObjectManager* objectManager = [RKObjectManager objectManagerWithBaseURL:webServiceLink];
    // Enable automatic network activity indicator management
    //[RKRequestQueue sharedQueue].showsNetworkActivityIndicatorWhenBusy = YES;
    // Initialize object store
    //objectManager.objectStore =  [RKManagedObjectStore objectStoreWithStoreFilename:@"JamTree.sqlite"];
    //usingSeedDatabaseName:[@"canalstreet.sqlite" managedObjectModel:nil];
    //[objectManager registerClass:[RKRestaurant class] forElementNamed:@"restaurant"];
    //[objectManager registerClass:[Category class] forElementNamed:@"menucategory"];
    //[objectManager registerClass:[MenuItems class] forElementNamed:@"menuItems"];
    //[objectManager registerClass:[RKOffers class] forElementNamed:@"offerList"];
//}
-(void)arrayCount:(int)val{
    arrCount=val;
    
}

- (void)saveContext
{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

#pragma mark - Core Data stack

// Returns the managed object context for the application.
// If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
- (NSManagedObjectContext *)managedObjectContext
{
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] init];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return _managedObjectContext;
}

// Returns the managed object model for the application.
// If the model doesn't already exist, it is created from the application's model.
- (NSManagedObjectModel *)managedObjectModel
{
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"JamTree DataModel" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

// Returns the persistent store coordinator for the application.
// If the coordinator doesn't already exist, it is created and the application's store added to it.
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"JamTree.sqlite"];
    
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        /*
         Replace this implementation with code to handle the error appropriately.
         
         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
         
         Typical reasons for an error here include:
         * The persistent store is not accessible;
         * The schema for the persistent store is incompatible with current managed object model.
         Check the error message to determine what the actual problem was.
         
         
         If the persistent store is not accessible, there is typically something wrong with the file path. Often, a file URL is pointing into the application's resources directory instead of a writeable directory.
         
         If you encounter schema incompatibility errors during development, you can reduce their frequency by:
         * Simply deleting the existing store:
         [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil]
         
         * Performing automatic lightweight migration by passing the following dictionary as the options parameter:
         @{NSMigratePersistentStoresAutomaticallyOption:@YES, NSInferMappingModelAutomaticallyOption:@YES}
         
         Lightweight migration will only work for a limited set of schema changes; consult "Core Data Model Versioning and Data Migration Programming Guide" for details.
         
         */
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}

#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}




@end
