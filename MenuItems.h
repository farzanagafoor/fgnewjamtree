//
//  MenuItems.h
//  JamTreeTabBasedApp
//
//  Created by mobytz004 on 25/07/13.
//  Copyright (c) 2013 Mobytz. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface MenuItems : NSManagedObject

@property (nonatomic, retain) NSNumber * catId;
@property (nonatomic, retain) NSNumber * deleted;
@property (nonatomic, retain) NSString * desc;
@property (nonatomic, retain) NSString * image;
@property (nonatomic, retain) NSNumber * itemId;
@property (nonatomic, retain) NSString * name;
@property (nonatomic,retain) NSNumber * price;

@end
