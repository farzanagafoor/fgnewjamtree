//
//  menuView.m
//  JamTreeMockApp
//
//  Created by Mobytz on 06/06/13.
//  Copyright (c) 2013 Mobytz. All rights reserved.
//

#import "menuView.h"
#import "MainMenuViewController.h"
#import "MainMenuSub1ViewController.h"
#import "AppDelegate.h"
#import "Global.h"
#import "UIImageView+WebCache.h"
#import "Category.h"
#import "MenuItems.h"
#import "UIButton+WebCache.h"

int i,j,tag=0;

@implementation menuView
@synthesize menuPage;
@synthesize objectMainMenuController;
@synthesize mView;
@synthesize mButton;
@synthesize sView;
@synthesize sButton;
@synthesize sLabel,ssLabel;
@synthesize objectSub1,sImgView,k;


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        // Initialization code
         i=0;
         tag = 0;
         objectMainMenuController  = nil;
         objectSub1                = nil;
        [[NSBundle mainBundle] loadNibNamed:@"MenuView" owner:self options:nil];
        [self addSubview:self.menuPage];
    }
    return self;
}

 

- (IBAction)menuButtonPressed:(id)sender
{
    AppDelegate *deleg = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    int tagValue = (long int)[sender tag];
    UIButton *btn = (UIButton*)sender;
    tag= btn.tag;
    deleg.ID= deleg.catIdArray[tag-1];
    [objectMainMenuController menuButtonSelected:btn.tag];
    
   }
-(IBAction)subMenuButtonPressed:(id)sender
{
    NSLog(@"its fine!!!!");
    int tagValue = (long int)[sender tag];
    [objectSub1 whenMenuButtonSelected:sender];
//    switch (tagValue) {
//        case 0:
//            [objectSub1 whenMenuButtonSelected:sender];
//            break;
//        case 1:
//            [objectSub2 whenMenuButtonSelected:sender];
//            break;
//        case 2:
//          //  [objectSub3 whenMenuButtonSelected:sender];
//            break;
//        case 3:
//            //[objectSub4 whenMenuButtonSelected:sender];
//            break;
//        case 5:
//            //[objectSub6 whenMenuButtonSelected:sender];
//            break;
//            default:
//            break;
    
            
   // }
    
    
    
}





-(IBAction) mainViewAnimating:(id)sender
{
    [UIView beginAnimations:@"start" context:nil];
    [UIView setAnimationDuration:.25];
   
     AppDelegate *deleg = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    [objectMainMenuController.HUD hide:YES];
    if (tag==nil) {
         tag=1;
    }

    NSMutableArray *nameArray = [deleg.categoryDictnry valueForKey:@"name"];
    NSMutableArray *imageArray = [deleg.categoryDictnry valueForKey:@"image"];
    deleg.catIdArray = [deleg.categoryDictnry valueForKey:@"catItem"];
    CGRect mvFrame,mbFrame,mlFrame;
     mvFrame.size.width  = 320;
     mvFrame.size.height = 140;
     mbFrame.size.width  = 130;
     mbFrame.size.height = 100;
    mlFrame.size.width = 120;
    mlFrame.size.height=20;
     mvFrame.origin.x    = -320;
     mvFrame.origin.y    = (mvFrame.size.height-10)*i+10;
     mView = [[UIView alloc] initWithFrame:mvFrame];
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:1];
    [mView setFrame:CGRectMake(0, mvFrame.origin.y, mvFrame.size.width, mvFrame.size.height)];
    [UIView commitAnimations];
    for (j=0; j<2; j++)
    {
        NSLog(@"tag::%d",tag);
         NSLog(@"%@",objectSub1.sub1ScrollView);
         mbFrame.origin.x = (mbFrame.size.width+15)*j +20;
         mbFrame.origin.y =  0;
        mlFrame.origin.x = (mlFrame.size.width+15)*j+ 25;
        mlFrame.origin.y = 100;
        mButton = [[UIButton alloc] initWithFrame:mbFrame];
        imageName = [imageArray[tag-1] stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
        imageName = [imageName stringByReplacingOccurrencesOfString:@".." withString:@""];
        imgpath = [imageUrl stringByAppendingString:imageName];
        url=[NSURL URLWithString:imgpath];
        imgData=[NSData dataWithContentsOfURL:url];
       [mButton setImageWithURL:url forState:UIControlStateNormal];
        [mButton setTag:tag];
        mLabel = [[UILabel alloc]initWithFrame:mlFrame];
        mLabel.text= [nameArray objectAtIndex:tag-1];
        [mLabel setTextColor:[UIColor blackColor]];
        mLabel.textAlignment=NSTextAlignmentCenter;
        [mLabel setBackgroundColor:[UIColor clearColor]];
        [mButton setBackgroundColor:[UIColor whiteColor]];
        [mButton addTarget:self action:@selector(menuButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        [mView addSubview:mButton];
        [mView addSubview:mLabel];
        [mView bringSubviewToFront:mLabel];
        tag++;
    }
    
         i++;
    NSLog(@"%d",deleg.count);
    if (i>[deleg.categoryDictnry count])
    {
        i   = 0;
        tag = 0;
    }
    [UIView commitAnimations];
}
-(void) subMenuAnimating:(int) value

{
    [UIView beginAnimations:@"start" context:nil];
    [UIView setAnimationDuration:.25];
  
    if (k==nil) {
        k=0;
    }
    AppDelegate *deleg = (AppDelegate*)[[UIApplication sharedApplication] delegate];
         NSLog(@"iii%d",i);
    CGRect mvFrame,imgFRame,sLblFrame,ssLabelFrame;
    mvFrame.size.width  = 320;
    mvFrame.size.height = 50;
    mvFrame.origin.x    = -320;
    mvFrame.origin.y    = (mvFrame.size.height+10)*i+50;
    imgFRame.size.width=75;
    imgFRame.size.height=50;
    imgFRame.origin.x    = -310;
    imgFRame.origin.y    = (mvFrame.size.height+10)*i+50;
    sLblFrame.size.width = 250;
    sLblFrame.size.height = 20;
    sLblFrame.origin.x =-280;
    sLblFrame.origin.y = (mvFrame.size.height+10)*i+75;
    ssLabelFrame.size.width = 50;
    ssLabelFrame.size.height = 20;
    ssLabelFrame.origin.x =280;
    ssLabelFrame.origin.y = (mvFrame.size.height)-18;
    NSMutableArray *array=[deleg.cachedDict valueForKey:@"image"];
     NSMutableArray *arrayName=[deleg.cachedDict valueForKey:@"name"];
    NSMutableArray *arrayPrice=[deleg.cachedDict valueForKey:@"price"];
    sView    = [[UIView alloc] initWithFrame:mvFrame];
     sButton  = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 320, 100)];
     sLabel   = [[UILabel alloc] initWithFrame:sLblFrame];
     ssLabel   = [[UILabel alloc] initWithFrame:ssLabelFrame];
    sImgView = [[UIImageView alloc] initWithFrame:imgFRame];
    NSString *imgstr=array[k];
    imageName = [imgstr stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    imageName = [imageName stringByReplacingOccurrencesOfString:@".." withString:@""];
    imgpath = [imageUrl stringByAppendingString:imageName];
    url=[NSURL URLWithString:imgpath];
    //imgData=[NSData dataWithContentsOfURL:url];
    [sImgView setImageWithURL:url];
    [sView setBackgroundColor:[UIColor blackColor]];
    [sView setAlpha:.5];
    [sLabel setAlpha:.5];
    [ssLabel setAlpha:.5];
    [sLabel setBackgroundColor:[UIColor clearColor]];
    [ssLabel setBackgroundColor:[UIColor clearColor]];
    [sLabel setTextColor:[UIColor whiteColor]];
    [ssLabel setTextColor:[UIColor whiteColor]];
    [sButton addTarget:self action:@selector(subMenuButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [sButton setTag:tag];
    [sLabel  setTag:tag];
    [sLabel setFont:[UIFont fontWithName:@"Arial" size:15]];
    [ssLabel  setTag:tag];
    [ssLabel setFont:[UIFont fontWithName:@"Arial" size:15]];
   
    sLabel.textAlignment = NSTextAlignmentLeft;
    ssLabel.textAlignment = NSTextAlignmentLeft;
    sLabel.text=arrayName[k];
    ssLabel.text=[@"£ " stringByAppendingString:[arrayPrice[k]stringValue]];
    [sView addSubview:sButton];
    [sView  addSubview:sImgView];
    [sView addSubview:ssLabel];
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:.35];
    [sView setFrame:CGRectMake(0, mvFrame.origin.y, mvFrame.size.width, mvFrame.size.height)];
    [sImgView setFrame:CGRectMake(10, mvFrame.origin.y, imgFRame.size.width, imgFRame.size.height)];
    [sLabel setFrame:CGRectMake(100, mvFrame.origin.y, mvFrame.size.width, mvFrame.size.height)];
    [UIView commitAnimations];
    i++;
    tag++;
    k++;
    if (i>([array count]/7)-1)
    {
        i   = 0;
        tag = 0;
        k = 0;
    }

    [UIView commitAnimations];
}


@end
