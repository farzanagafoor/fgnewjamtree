//
//  MainMenuSub1ViewController.h
//  JamTreeMockApp
//
//  Created by Mobytz on 07/06/13.
//  Copyright (c) 2013 Mobytz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@class menuView;
@interface MainMenuSub1ViewController : UIViewController<UIScrollViewDelegate>
{
    menuView       *objectMenu;
    NSArray        *labelArray;
    int count;
    int value1;
    AppDelegate *deleg;

}
@property(nonatomic)int n;
@property (nonatomic)int value1;
@property (nonatomic)int count;
@property (nonatomic, retain) menuView       *objectMenu;
@property (weak, nonatomic) IBOutlet UIScrollView *sub1ScrollView;

- (IBAction)backButtonPressed:(id)sender;
-(void) whenMenuButtonSelected:(int)value;
-(void) menuAppear:(int)value;
-(void)fetch;
@end
