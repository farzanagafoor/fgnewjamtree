//
//  WebService.h
//  bionicPhotoManager
//
//  Created by Zachariah on 11/03/2013.
//  Copyright (c) 2013 Kiran Babu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ASIFormDataRequest.h"
#import "CJSONDeserializer.h"
#import "Global.h"

typedef enum
{    
    REQAPPDETAILS
} REQCallType;


@interface WebService : NSObject
{
    ASIFormDataRequest *request;
    REQCallType apiCallType;
    NSError *theError;
    BOOL finished;
    NSString *str;
    
}
@property(nonatomic,retain) NSString *arr;
@property (nonatomic, assign) REQCallType apiCallType;
-(void) fetchMenu;
@property BOOL finished;
@end
